<?php

return [
    'phase_one_ticker_price'            => 0.05,
    'phase_two_ticker_price'            => 0.08,
    'phase_one_duration_day'            => 15,
    'phase_two_duration_day'            => 30,
    'company_commission_rate'           => 0.1,
    'iso_initiator_commission_rate'     => 0.3,
    'dividend_commission_rate'          => 0.1,
    'phase_one_producible_ticker_count' => 1000000,
    'phase_two_producible_ticker_count' => 10000000,

];
