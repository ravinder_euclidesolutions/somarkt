<?php 
 
namespace App\Mail;

use App\Template;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

class AdminEventNotify extends Mailable
{
    use Queueable, SerializesModels;

    public $template;
      
    public function __construct($summary)
    {
        $this->template = '';
        $this->template .= '<h2>Here are the list of Event Notification</h2>';
        $this->template .= '<table border="1" width="100%" cellspacing="0" cellpadding="10">';
        $this->template .= '<thead><tr>
            <th align="left" style="color:#000">Sr.No</th>
            <th align="left" style="color:#000">Account</th>
            <th align="left" style="color:#000">Event</th>
            <th align="left" style="color:#000">Event Date</th>
            </tr></thead><tbody>';

        foreach ($summary as $key => $info) {
            $this->template .= '<tr>';
            $this->template .= '<td style="color:#000">'.++$key.'</td>';
            $this->template .= '<td style="color:#000">'.$info['account'].'</td>';
            $this->template .= '<td style="color:#000">'.$info['title'].'</td>';
            $this->template .= '<td style="color:#000">'.$info['eventstart'].'</td>';
            $this->template .= '</tr>';
        }
        $this->template .= '</tbody></table>';
    }
    
    public function build()
    {
        return $this->markdown('emails.payroll');
    }
}
