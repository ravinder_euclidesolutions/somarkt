<?php 
 
namespace App\Mail;

use App\Template;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

class QuickbooksReport extends Mailable
{
    use Queueable, SerializesModels;
 
    public $template;
     
    public function __construct()
    {    
        $email_template = Template::find(7); 
 
        $this->template = $email_template->template_payload;         
    }
    
    public function build()
    {
        return $this->markdown('emails.quickbooks');
    }
}
