<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactNotify extends Mailable
{
    use Queueable, SerializesModels;

    public $info;

    public function __construct($infodata)
    {
        $this->user  = $infodata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->markdown('email.welcome-user');
    }
}
