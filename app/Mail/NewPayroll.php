<?php 
 
namespace App\Mail;

use App\Template;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

class NewPayroll extends Mailable
{
    use Queueable, SerializesModels;

    public $payroll;

    public $template;
     
    public function __construct($payrolldata)
    {

        $this->payroll   = $payrolldata;        
        $email_template = Template::find(4); 

        $this->template = $email_template->template_payload;
         
    }
    
    public function build()
    {
        return $this->markdown('emails.payroll');
    }
}
