<?php
 
namespace App\Mail;

use App\Template;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

class NewReport extends Mailable
{
    use Queueable, SerializesModels;

    public $report;

    public $template;
     
    public function __construct($reportdata)
    {

        $this->report   = $reportdata;        
        $email_template = Template::find(3);
        

        $search = array (
            '[Report Title]', 
        );

        $replace = array (
            $this->report['report_name'],            
        );

        $this->template = str_replace ( $search, $replace, $email_template->template_payload );
         
    }
    
    public function build()
    {

        return $this->markdown('emails.report') 
        ->subject('New Report')
        ->attach(request()->file('report_path')->getRealPath(),
        [
            'as'   => 'Report.pdf',
            'mime' => 'application/pdf',
        ]); 
        
    }
}
