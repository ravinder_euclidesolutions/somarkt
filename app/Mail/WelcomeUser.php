<?php
 
namespace App\Mail;

use App\Template;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $template;
     
    public function __construct($userdata)
    {
        $this->user     = $userdata;        
        $email_template = Template::find(2);
        $search = array (
            '[username]',
            '[sitename]',
            '[useremail]',
            '[password]' 
        );
        $replace = array (
            $this->user['name'],
            config('app.url'),
            $this->user['mail'],
            $this->user['password'] 
        );

        $this->template = str_replace ( $search, $replace, $email_template->template_payload );
    }
    
    public function build()
    {
        return $this->markdown('emails.welcome-user');
    }
}
