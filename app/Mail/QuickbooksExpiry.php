<?php 
 
namespace App\Mail;

use App\Template;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

class QuickbooksExpiry extends Mailable
{
    use Queueable, SerializesModels;
 
    public $template;
    
    public $account;

    public function __construct($qaccount)
    {    
        $email_template = Template::find(6); 

        $this->account = $qaccount;

        $search = array (
            '[account]',            
        );

        $replace = array (
           $this->account->company_name,            
        );

        $this->template = str_replace ( $search, $replace, $email_template->template_payload );         
    }
    
    public function build()
    {
        return $this->markdown('emails.quickbooks');
    }
}
