<?php 
 
namespace App\Mail;

use App\Template;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Contracts\Queue\ShouldQueue;

class NewEvent extends Mailable
{
    use Queueable, SerializesModels;

    public $template;
    public $event;
     
    public function __construct($eventinfo)
    {
        $email_template = Template::find(1); 
        
        $this->event = $eventinfo;

        $search = array (
            '[firstname]',
            '[account]',
            '[eventtype]',
            '[description]',
            '[eventstart]',
        );

        $replace = array (
            $this->event['name'],
            $this->event['account'],
            $this->event['eventtype'],
            $this->event['description'],                        
            $this->event['eventstart'],                        
        );
        
        $this->template = str_replace ( $search, $replace, $email_template->template_payload );
         
    }
    
    public function build()
    {
        return $this->markdown('emails.event');
    }
}
