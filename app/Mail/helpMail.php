<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class helpMail extends Mailable
{
    use Queueable, SerializesModels;

    public $helpdata;
    public $template;

    public function __construct($contactinfo)
    {
        $this->helpdata   = $contactinfo;

        $this->template = '<p><b>Name:</b> '.$this->helpdata['name'].'</p>';
        $this->template .= '<p><b>Email:</b> '.$this->helpdata['email'].'</p>';
        $this->template .= '<p><b>Message:</b> '.$this->helpdata['message'].'</p>';

    }


    public function build()
    {
        return $this->markdown('emails.helpSendMail');
    }
}
