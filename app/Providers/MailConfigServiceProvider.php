<?php

namespace App\Providers;
   
use Config;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('settings')) {

            // Mailgun settings

            $MAIL_DRIVER         = DB::table('settings')->where('setting_name', 'MAIL_DRIVER')->get();            
            $MAIL_FROM_ADDRESS   = DB::table('settings')->where('setting_name', 'MAIL_FROM_ADDRESS')->get();
            $MAIL_FROM_NAME      = DB::table('settings')->where('setting_name', 'MAIL_FROM_NAME')->get();
            $MAILGUN_DOMAIN      = DB::table('settings')->where('setting_name', 'MAILGUN_DOMAIN')->get();
            $MAILGUN_SECRET      = DB::table('settings')->where('setting_name', 'MAILGUN_SECRET')->get();

            // Amazon s3 service provider
             $AWS_ACCESS_KEY_ID      = DB::table('settings')->where('setting_name', 'S3_KEY')->get();
             $AWS_SECRET_ACCESS_KEY  = DB::table('settings')->where('setting_name', 'S3_SECRET')->get();
             $AWS_DEFAULT_REGION     = DB::table('settings')->where('setting_name', 'S3_REGION')->get();
             $AWS_BUCKET             = DB::table('settings')->where('setting_name', 'S3_BUCKET')->get();

            // logzio tokken
            $LOGZ_TOKEN             = DB::table('settings')->where('setting_name', 'LOGZ_TOKEN')->get();

            // FTP Settings
            $FTP_HOST      = DB::table('settings')->where('setting_name', 'FTP_HOST')->get();
            $FTP_USERNAME  = DB::table('settings')->where('setting_name', 'FTP_USERNAME')->get();
            $FTP_PASSWORD  = DB::table('settings')->where('setting_name', 'FTP_PASSWORD')->get();
 
                 
            // set mail configuration  

            Config::set('mail', array_merge(config('mail'), [
                'driver' => $MAIL_DRIVER[0]->setting_value,
                'from' => [
                    'address' => $MAIL_FROM_ADDRESS[0]->setting_value,
                    'name' => $MAIL_FROM_NAME[0]->setting_value, 
                ]
            ]));

            Config::set('services', array_merge(config('services'), [
                'mailgun' => [
                    'domain' => $MAILGUN_DOMAIN[0]->setting_value,
                    'secret' => $MAILGUN_SECRET[0]->setting_value,
                ]
            ]));

            

            // set s3 configuration
            
            $bucketname   = $AWS_BUCKET[0]->setting_value;
            $bucketregion = $AWS_DEFAULT_REGION[0]->setting_value;
 
            config(['filesystems.disks.s3.key'    => $AWS_ACCESS_KEY_ID[0]->setting_value]); 
            config(['filesystems.disks.s3.secret' => $AWS_SECRET_ACCESS_KEY[0]->setting_value]); 
            config(['filesystems.disks.s3.region' => $AWS_DEFAULT_REGION[0]->setting_value]); 
            config(['filesystems.disks.s3.bucket' => $AWS_BUCKET[0]->setting_value]); 
            config(['filesystems.disks.s3.url'    => "https://$bucketname.s3-$bucketregion.amazonaws.com"]); 
             
            // set logz configuration
            config(['logging.channels.fixeinfo.token' => $LOGZ_TOKEN[0]->setting_value]); 
            
            // set FTP configuration
            config(['filesystems.disks.custom-ftp.host'      => $FTP_HOST[0]->setting_value]); 
            config(['filesystems.disks.custom-ftp.username'  => $FTP_USERNAME[0]->setting_value]); 
            config(['filesystems.disks.custom-ftp.password'  => $FTP_PASSWORD[0]->setting_value]); 
             
            
        }
    }

    
}
