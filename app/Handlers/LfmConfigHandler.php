<?php

namespace App\Handlers;
use App\User; 
use App\UserDetail;

class LfmConfigHandler extends \UniSharp\LaravelFilemanager\Handlers\ConfigHandler
{
    public function userField()
    {
       
       // check for role
        $role = auth()->user()->roles()->first();

	    if($role->name == 'admin') {

	        return 'files';

	    } else if ($role->name == 'owner'){

	    	$id = auth()->user()->id; 

	    	$account = UserDetail::select('user_details.company_name')
    		       ->where('user_details.user_id',$id)->first();

	        return 'files/' . $account->company_name;          

	    } else {

	    	$user = auth()->user();

	    	$account = UserDetail::select('user_details.company_name')
    			       ->where('user_details.user_id',$user->customer_id)->first();

	        return 'files/' . $account->company_name;

	    }

    }

    public function baseDirectory() {

       $role = auth()->user()->roles()->first();

	    if($role->name == 'admin') {
	    	
	        return 'files';

	    } else if ($role->name == 'owner'){

	    	$id = auth()->user()->id; 

	    	$account = UserDetail::select('user_details.company_name')
    			       ->where('user_details.user_id',$id)->first();

	        return 'files/' . $account->company_name;          

	    } else {

	    	$user = auth()->user();

	    	$account = UserDetail::select('user_details.company_name')
    			       ->where('user_details.user_id',$user->customer_id)->first();

	        return 'files/' . $account->company_name;

	    }
    }


}
 