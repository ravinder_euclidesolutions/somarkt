<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use App\Setting;
use App\User;
use App\UserDetail;
use App\Quickbooks;
use Illuminate\Http\Request;
use QuickBooksOnline\API\Security\OAuthRequestValidator; 
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;
use QuickBooksOnline\API\Utility\Configuration;
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Data\IPPPurchase;
use QuickBooksOnline\API\QueryFilter\QueryMessage;
use QuickBooksOnline\API\ReportService\ReportService;
use QuickBooksOnline\API\ReportService\ReportName;
use Illuminate\Support\Facades\Log;
use Laravel\Logzio\LogzioServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File; 

class Fixecrunchtimereport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixe:crunchtimereport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Will Upload Quickbooks Reports on FTP Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $Auth_Mode      =  DB::table('settings')->where('setting_name', 'Auth_Mode')->first();
        $Client_ID      =  DB::table('settings')->where('setting_name', 'Client_ID')->first();
        $Client_Secret  =  DB::table('settings')->where('setting_name', 'Client_Secret')->first();
        $Redirect_URI   =  DB::table('settings')->where('setting_name', 'Redirect_URI')->first();
        $Scope          =  DB::table('settings')->where('setting_name', 'Scope')->first();
        $Base_Url       =  DB::table('settings')->where('setting_name', 'Base_Url')->first();
        
        //get quickbooks config
        $quickbooks = Quickbooks::where('realmID','9130347705812686')->first();  
        
        // get current date 
        $currentDate = date("Y/m/d H:i:s");
 
        // check if access tokken expired or not
        if($currentDate > $quickbooks->accessTokenExpiry) { 
               
                // if tokken expired get new value
                $oauth2LoginHelper = new OAuth2LoginHelper($Client_ID->setting_value,$Client_Secret->setting_value);
                $accessTokenObj     = $oauth2LoginHelper->refreshAccessTokenWithRefreshToken($quickbooks->refreshTokenValue);
                $refreshTokenValue  = $accessTokenObj->getRefreshToken();
                $refreshTokenExpiry = $accessTokenObj->getRefreshTokenExpiresAt();      
                $accessTokenValue   = $accessTokenObj->getAccessToken();
                $accessTokenExpiry  = $accessTokenObj->getAccessTokenExpiresAt();
                 
                $quickbooks->update([               
                    'refreshTokenValue'     => $refreshTokenValue,
                    'refreshTokenExpiry'    => $refreshTokenExpiry,
                    'accessTokenValue'      => $accessTokenValue,
                    'accessTokenExpiry'     => $accessTokenExpiry,           
                ]);  
                
               $dataService = DataService::Configure(array (
                    'auth_mode'    => $Auth_Mode->setting_value,
                    'ClientID'     => $Client_ID->setting_value,
                    'ClientSecret' => $Client_Secret->setting_value,
                    'RedirectURI'  => $Redirect_URI->setting_value,
                    'scope'        => $Scope->setting_value, 
                    'QBORealmID'   => $quickbooks->realmID,
                    'baseUrl'      => $Base_Url->setting_value, 
                    'accessTokenKey'  => $quickbooks->accessTokenValue,
                    'refreshTokenKey' => $quickbooks->refreshTokenValue,                
                ));
            
        } else {
                
            $dataService = DataService::Configure(array (
                'auth_mode'    => $Auth_Mode->setting_value,
                'ClientID'     => $Client_ID->setting_value,
                'ClientSecret' => $Client_Secret->setting_value,
                'RedirectURI'  => $Redirect_URI->setting_value,
                'scope'        => $Scope->setting_value, 
                'QBORealmID'   => $quickbooks->realmID,
                'baseUrl'      => $Base_Url->setting_value, 
                'accessTokenKey'  => $quickbooks->accessTokenValue,
                'refreshTokenKey' => $quickbooks->refreshTokenValue,                
            ));
        }
         
        
        $serviceContext = $dataService->getServiceContext();
        // Prep Data Services
        $reportService = new ReportService($serviceContext);
        $reportService->setCustomer('588');
        $reportService->setDateMacro("Today"); 
        $newReport = $reportService->executeReport('TransactionList'); 
            
        $details = [];
        $product = [];
        $filedata = '';
        
        // create text file
        
        $currentdate = date('Ymd');
        $filename = '009_sweetgreen_'.$currentdate.'.txt';
        $file = fopen(base_path().'/public/csv/'.$filename, 'wb');  

        foreach($newReport->Rows->Row as $rowdata) {
            
            // get locations
            foreach ($rowdata->ColData as $inkey => $inrdata)  {
                 
                if($inkey == 0) { $d = $inrdata->value; $details['date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $d)->format('m/d/Y');}
                if($inkey == 1) { $details['invoice_id'] = $inrdata->id; }
                if($inkey == 3 || $inkey == 5 || $inkey == 6 || $inkey == 7) { continue; }
                if($inkey == 2) { $details['nvoice_num'] = $inrdata->value; }
                if($inkey == 4) { 
                    $loc_name = $inrdata->value; 
                    $details['location']  = substr($loc_name, 37, strpos($loc_name, ":")); 
                }
                if($inkey == 8) { $details['amount']     = $inrdata->value; }
            }
            
            $filedata .= "H\t0485\t".trim($details['location'])."\t\t\t".trim($details['nvoice_num'])."\t".trim($details['date'])."\t".trim($details['amount'])."\n";                                
            // insert header info in the file
            fwrite($file, $filedata); 
            $filedata = '';
            $invoice = $dataService->FindbyId('invoice', $details['invoice_id']);
            
            // get location product details
            foreach($invoice->Line as $prokey => $product_head) {
               
                if ($prokey === array_key_last($invoice->Line)) { 
                    continue;
                }
                $product[$prokey]['product_name']   = $product_head->Description;
                $product[$prokey]['product_amount'] = $product_head->Amount;
                $product[$prokey]['product_price']  = $product_head->SalesItemLineDetail->UnitPrice;
                $product[$prokey]['product_qty']  = $product_head->SalesItemLineDetail->Qty;
            }
           
            // insert product info in the file
            foreach($product as $proinfo) {
              $filedata .= "D\t"."Bread:".$proinfo['product_name']. "\t\t".$proinfo['product_qty'] ."\t". $proinfo['product_price'] ."\t". $proinfo['product_amount']."\n";
              fwrite($file, $filedata);
              $filedata = '';
            }
        }
        
        fclose($file);
        
        $file = fopen(base_path().'/public/csv/'.$filename, 'r+');    
 
        if(Storage::disk('custom-ftp')->put($filename,$file, 'public')) {

            $user = User::where('id', $quickbooks->user_id)->first();
            // send welcome mail
            \Mail::to($user)->send(new \App\Mail\QuickbooksReport());

            fclose($file);

            $file_path = base_path().'/public/csv/'.$filename;
                
            // delete file
            File::delete($file_path);

            Log::info("Crunchtime Report Uploaded to FTP server Successfully");
            
        }  
         
       
    }
}
