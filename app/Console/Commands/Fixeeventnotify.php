<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use App\Setting;
use App\User;
use App\Event;
use App\UserDetail;
use App\Quickbooks;
use Illuminate\Http\Request;
use QuickBooksOnline\API\Security\OAuthRequestValidator; 
use Illuminate\Support\Facades\Log;
use Laravel\Logzio\LogzioServiceProvider;
use Illuminate\Support\Facades\DB;

class Fixeeventnotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixe:eventnotifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Will Notify User for Calander Events';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $events = Event::whereDate('eventstart', '>', \Carbon\Carbon::now())->get();
        $admins = [];

        foreach ($events as $event) {
             // get current date 
            $currentDate = date("Y-m-d H:i:s");
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $event->eventstart);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $currentDate);
            $diff_in_days = $to->diffInDays($from);
            
            if($diff_in_days == $event->days_before) {

                $user = User::where('id',$event->user_id)->first();
                $admin = User::where('id',$event->admin_id)->first();
                $account = UserDetail::where('user_id',$event->user_id)->first(); 

                $eventinfo = [
                   'name'        => $user->name,                  
                   'account'     => $account->company_name,
                   'eventtype'   => $event->title,
                   'description' => $event->description,
                   'eventstart'  => $event->eventstart,
                ];

                if(!empty($admins) && array_key_exists($admin->id, $admins)) {
                    array_push($admins[$admin->id], array(                            
                            'account' => $account->company_name,
                            'title' => $event['title'], 
                            'eventstart'  => $event->eventstart,
                        )
                    );
                } else {
                    $admins[$admin->id] = array(array(                         
                        'account' => $account->company_name,
                        'title' => $event['title'],  
                        'eventstart'  => $event->eventstart,
                    ));
                }

                // send event mail
               \Mail::to($user)->send(new \App\Mail\NewEvent($eventinfo));

                Log::info("Event Notify Mail Sent to: $user->name");
            }
        }
      
        $summary = [];

        foreach ($admins as $key => $einfo) {
            
            foreach ($einfo as $newkey => $innerInfo) {
                
                $summary[$newkey] = array(
                    'account' =>  $innerInfo['account'],
                    'title'   =>  $innerInfo['title'],
                    'eventstart' => $innerInfo['eventstart'],
                );              
            }  

            $adminNotify = User::whereHas('roles' , function($q){
                $q->where('name', 'admin');
            })->get(); 

            // send event list mail
            \Mail::to($adminNotify)->send(new \App\Mail\AdminEventNotify($summary));

        }
 
         
    }
}