<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use App\Setting;
use App\User;
use App\UserDetail;
use App\Quickbooks;
use Illuminate\Http\Request;
use QuickBooksOnline\API\Security\OAuthRequestValidator; 
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;
use QuickBooksOnline\API\Utility\Configuration;
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Data\IPPPurchase;
use QuickBooksOnline\API\QueryFilter\QueryMessage;
use QuickBooksOnline\API\ReportService\ReportService;
use QuickBooksOnline\API\ReportService\ReportName;
use Illuminate\Support\Facades\Log;
use Laravel\Logzio\LogzioServiceProvider;
use Illuminate\Support\Facades\DB;

class Fixequickbooksnotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixe:quickbooksnotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Will Notify User for quickbooks token expiry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        //get quickbooks config
        $quickbooks = Quickbooks::all();
        
        foreach ($quickbooks as  $quickaccount) {
             // get current date 
        $currentDate = date("Y/m/d H:i:s");

            $to = \Carbon\Carbon::createFromFormat('Y/m/d H:s:i', $quickaccount->refreshTokenExpiry);
            $from = \Carbon\Carbon::createFromFormat('Y/m/d H:s:i', $currentDate);
            $diff_in_days = $to->diffInDays($from);
            
            if($diff_in_days < 3) {
         
                $qaccount = UserDetail::where('user_id',$quickaccount->user_id)
                ->select('company_name')->first();         

                $admins = User::whereHas('roles' , function($q){
                    $q->where('name', 'admin');
                })->get();

                // send welcome mail
                \Mail::to($admins)->send(new \App\Mail\QuickbooksExpiry($qaccount));

                Log::info("Quickbooks Token Expiry Mail Sent Successfully");

            }
        }
       
         
       
    }
}
