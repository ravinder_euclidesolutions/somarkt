<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IsoResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->resource->uuid,
            'name'        => $this->resource->name,
            'code'        => $this->resource->code,
            'description' => $this->resource->description,
            'status'      => $this->resource->status,
            'is_active'   => $this->resource->is_active,
            'user'        => $this->resource->user,
            'phase'       => $this->resource->activePhase()
        ];
    }
}
