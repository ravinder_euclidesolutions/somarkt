<?php

namespace App\Http\Controllers\FrontControllers;

use App\Http\Controllers\FrontControllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Hashids\Hashids;
use Illuminate\Support\Facades\Hash;
use Storage;

class ProfileController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $hashids = new Hashids('', 12);
        $id = $hashids->decode($id)[0];
        $user = User::findOrFail($id);
        return view('profile.show',compact(['hashids','user']));
    }


    public function edit($id)
    {
        $hashids = new Hashids('', 12);
        $id = $hashids->decode($id)[0];

        $user = User::findOrFail($id);
        return view('profile.edit',compact(['hashids','user']));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['confirmed'],
            'avatar'   => ['image','mimes:jpeg,png,jpg,gif,svg','max:2048'],
        ]);

        $user = User::where("id",$id)->first();

        $user->name        = request('name');
        $user->surname     = request('surname');
        $user->email       = request('email');
        $user->phone       = request('phone');
        $user->ETH_address = request('ETH_address');
        $user->save();

        if(request('password')) {
            $user->update([
                'password'    => request('password'),
            ]);
        }

        if(request()->hasFile('avatar')) {

            $filenamewithextension = request()->file('avatar')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = request()->file('avatar')->getClientOriginalExtension();
            $filenametostore = "images/profiles/".$filename.'_'.time().'.'.$extension;
            Storage::disk('public_uploads')->put($filenametostore, fopen(request()->file('avatar'), 'r+'));

            $user->update([
                'avatar'  => $filenametostore,
            ]);
        }

        return redirect('/');
    }


    public function destroy($id)
    {
        //
    }
}
