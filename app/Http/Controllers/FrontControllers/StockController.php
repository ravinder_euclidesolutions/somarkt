<?php

namespace App\Http\Controllers\FrontControllers;

use App\Enums\ShareStatusType;
use App\Exceptions\IsoIsNotBuyableException;
use App\Http\Controllers\Controller;
use App\Models\Commission;
use App\Models\Iso;
use App\Models\Share;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StockController extends Controller {

    /**
     * @param Request $request
     * @return mixed
     * @throws \Throwable
     */
    public function buy(Request $request)
    {
        $user = Auth::user();

        $stock_amount = $request->get('count');
        $iso = Iso::findOrFailByUuid($request->get('iso'));
        $phase = $iso->activePhase()->first();

        throw_if(!$this->isoIsBuyable($phase, $stock_amount), IsoIsNotBuyableException::class);

        $total = $stock_amount * $phase->phase->stock_price;
        $commissions = Commission::all();

        if ($phase->order == 1)
        {
            $grossTotal = $total;
        } else
        {
            $grossTotal = $total;
            foreach ($commissions as $commission)
            {
                $grossTotal += $total * $commission->commission;
            }
        }

        /** @var Share $share */
        $share = $user->shares()->create([
            'iso_id'          => $iso->id,
            'phase_id'        => $phase->id,
            'amount'          => $stock_amount,
            'price_per_stock' => $phase->phase->stock_price,
            'total'           => $total,
            'gross_total'     => $grossTotal,
            'status'          => ShareStatusType::InProgress,
        ]);

        $iso->phases()->updateExistingPivot($phase->id, [
            'shareable_stock' => $phase->phase->shareable_stock - $stock_amount,
        ]);

        if ($phase->order > 1 && $commissions->count() > 0)
        {
            foreach ($commissions as $commission)
            {
                $share->commissions()->attach($commission->id, ['commission' => $commission->commission]);
            }
        }

        return response()->success(__('iso.buy_stock_successful'));
    }

    /**
     * @param $phase
     * @param $amount
     * @return bool
     */
    protected function isoIsBuyable($phase, $amount)
    {
        if (is_null($phase))
            return false;

        return $phase->phase->shareable_stock > $amount;
    }
}
