<?php

namespace App\Http\Controllers\FrontControllers;

use App\Http\Controllers\FrontControllers\Controller;
use Illuminate\Http\Request;
use App\Enums\IsoStatusType;
use App\Models\Iso;
use App\Models\Phase;
use App\Models\User;
use Hashids\Hashids;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\IsoRequest;
use App\Http\Resources\IsoResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Carbon;


class ISOController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        $hashids = new Hashids('', 12);

        return view('iso.create',compact('hashids'));
    }


    public function store(Request $request)
    {
        $user = Auth::user();
        $tags = $request->get('tags');
        $phase = Phase::where('order', 2)->first();


        $iso = $user->iso()->create([
            'name'        => $request->get('name'),
            'code'        => $request->get('code'),
            'description' => $request->get('description'),
            'status'      => IsoStatusType::WaitingApproval,
            'is_active'   => true,
        ]);

        $iso->phases()->attach($phase->id, [
            'shareable_stock' => $phase->shareable_stock,
            'stock_price'     => $phase->stock_price,
            'expire_at'       => Carbon::now()->addDays($phase->duration)
        ]);

        if (!is_null($tags))
        {
            $tagList = explode(',', $tags);
            $iso->attachTags($tagList);
        }
        $hashids = new Hashids('', 12);
        $message = 'Successfully saved.';
        return view('iso.create',compact('hashids','message'));
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function list()
    {
        $hashids = new Hashids('', 12);
        $user = Auth::user();
        $list = $user->iso()->get();
        return view('iso.list',compact('list','hashids','user'));
    }
}
