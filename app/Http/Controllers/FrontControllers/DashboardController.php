<?php

namespace App\Http\Controllers\FrontControllers;

use App\Http\Controllers\FrontControllers\Controller;
use Illuminate\Http\Request;
use Hashids\Hashids;
use App\Models\User;
use App\Models\Contact;
use App\Mail\helpMail;
class DashboardController extends Controller
{

    public function index()
    {
        $hashids = new Hashids('', 12);

        $users = User::whereHas("roles", function($q){ $q->where("name", "=", "iso-initiator"); })
        ->orderBy('id','DESC')->get();

        return view('home',compact(['users','hashids']));
    }

    public function privacy()
    {   $hashids = new Hashids('', 12);
        return view('generals.privacy',compact(['hashids']));
    }

    public function terms()
    {   $hashids = new Hashids('', 12);
        return view('generals.terms',compact(['hashids']));
    }

    public function howto()
    {   $hashids = new Hashids('', 12);
        return view('generals.howto',compact(['hashids']));
    }

    public function help()
    {   $hashids = new Hashids('', 12);
        return view('generals.contact',compact(['hashids']));
    }

    public function storeHelp()
    {

        request()->validate([
            'name'      => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255'],
            'message'   => ['required'],
        ]);

        Contact::create([
            'name'    => request('name'),
            'email'   => request('email'),
            'message' => request('message'),
        ]);

        $contactinfo = [
            'name'     => request('name'),
            'email'     => request('email'),
            'message'  => request('message')
         ];

        $admins = User::whereHas('roles' , function($q){
            $q->where('name', 'super-admin');
        })->get();

        // send welcome mail
        \Mail::to($admins)->send(new helpMail($contactinfo));

        return redirect()->back()->with('success','Submitted Successfully');

    }



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
