<?php

namespace App\Http\Controllers\FrontControllers\Auth;

use App\Http\Controllers\FrontControllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use App\Models\Iso;
use App\Models\Phase;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Enums\IsoStatusType;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $security = "somarkt_2021";

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'security_code' => ['required'],
            'security_code' => 'required|in:'.$security,
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'surname'  => $data['surname'],
            'phone'    => $data['phone'],
            'ETH_address' => $data['ETH_address'],
            'password' => $data['password'],
        ])->assignRole('iso-initiator');

        if (isset($fields['wallet_address'])) {
            $user->cryptoWallets()->create([
                'address'   => $fields['wallet_address'],
                'type'      => CryptoWalletType::Eth,
                'is_active' => true,
            ]);
        }
        $this->storeIso($user, $data);

        return $user;

    }

    protected function redirectTo()
    {
        return '/';
    }


   protected function storeIso($user, $data)
    {
        $tags = $data['tags'];
        $phase = Phase::where('order', 1)->first();


        $iso = $user->iso()->create([
            'name'        => $data['name'],
            'code'        => $data['code'],
            'description' => $data['description'],
            'status'      => IsoStatusType::WaitingApproval,
            'is_active'   => true,
        ]);

        $iso->phases()->attach($phase->id, [
            'shareable_stock' => $phase->shareable_stock,
            'stock_price'     => $phase->stock_price,
            'expire_at'       => Carbon::now()->addDays($phase->duration)
        ]);

        if (!is_null($tags))
        {
            $tagList = explode(',', $tags);
            $iso->attachTags($tagList);
        }
        return true;
    }
}
