<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\IsoStatusType;
use App\Http\Controllers\FrontControllers\Controller;
use App\Http\Requests\IsoRequest;
use App\Http\Resources\IsoResource;
use App\Models\Iso;
use App\Models\Phase;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class IsoController extends Controller {

    public function __construct()
    {
        $this->middleware(['role:super-admin'])->only('store');
    }

    public function index(Request $request): AnonymousResourceCollection
    {
        $search = $request->get('search');

        $iso = Iso::search($search)->paginate();

        return IsoResource::collection($iso);
    }

    /**
     * @param IsoRequest $request
     * @return mixed
     */
    public function store(IsoRequest $request)
    {
        $user = Auth::user();
        $tags = $request->get('tags');
        $phase = Phase::where('order', 2)->first();

        /** @var Iso $iso */
        $iso = $user->iso()->create([
            'name'        => $request->get('name'),
            'code'        => $request->get('code'),
            'description' => $request->get('description'),
            'status'      => IsoStatusType::WaitingApproval,
            'is_active'   => true,
        ]);

        $iso->phases()->attach($phase->id, [
            'shareable_stock' => $phase->shareable_stock,
            'stock_price'     => $phase->stock_price,
            'expire_at'       => Carbon::now()->addDays($phase->duration)
        ]);

        if (!is_null($tags))
        {
            $tagList = explode(',', $tags);
            $iso->attachTags($tagList);
        }

        return response()->success(__('iso.successfully_created'));
    }
}
