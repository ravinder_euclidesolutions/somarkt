<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\FrontControllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function me()
    {
        return Auth::user();
    }
}
