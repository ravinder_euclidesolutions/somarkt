<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\CryptoWalletType;
use App\Http\Controllers\FrontControllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller {

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function login(Request $request): array
    {
        $request->validate([
            'email'       => 'required|email',
            'password'    => 'required',
            'device_name' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password))
        {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return [
            'status'     => 'success',
            'token'      => $user->createToken($request->device_name)->plainTextToken,
            'token_type' => 'Bearer'
        ];
    }

    /**
     * @param RegisterRequest $request
     * @return mixed
     */
    public function register(RegisterRequest $request)
    {
        $fields = $request->only(['name', 'surname', 'email', 'phone', 'password', 'wallet_address']);

        /** @var User $user */
        $user = User::create($fields);

        if (isset($fields['wallet_address'])) {
            $user->cryptoWallets()->create([
                'address'   => $fields['wallet_address'],
                'type'      => CryptoWalletType::Eth,
                'is_active' => true,
            ]);
        }

        return response()->success(__('user.registration_successful'));
    }
}
