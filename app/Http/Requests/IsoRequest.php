<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IsoRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|min:5|max:255',
            'code'        => 'required|min:4|max:4|unique:iso',
            'description' => 'max:5000'
        ];
    }
}
