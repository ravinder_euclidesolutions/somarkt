<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

/**
 * Trait HasUuid
 *
 * @property string uuid Str::uuid();
 *
 * @package App\Traits
 */
trait HasUuid
{
    protected static function bootHasUuid()
    {
        static::creating(function ($model) {
            $model->uuid = Str::uuid();
        });
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * @param string $uuid
     * @return Builder|self
     *
     */
    public static function findByUuid($uuid)
    {
        return static::where('uuid', $uuid)
            ->first();
    }

    /**
     * @param string $uuid
     * @throws ModelNotFoundException
     * @return Builder|self
     */
    public static function findOrFailByUuid($uuid)
    {
        return static::where('uuid', $uuid)
            ->firstOrFail();
    }

    /**
     * @param string $uuid
     * @param $data
     * @return int
     */
    public static function updateByUuid($uuid, $data)
    {
        return static::where('uuid', $uuid)
            ->update($data);
    }

    /**
     * @param \Illuminate\Support\Collection|array $uuids
     * @return Collection
     */
    public static function findByUuidArray($uuids)
    {
        return static::whereIn('uuid', $uuids)
            ->get();
    }
}
