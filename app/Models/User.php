<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'name',
        'surname',
        'email',
        'password',
        'phone',
        'ETH_address',
        'status',
        'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
		'account_verified_at' => 'datetime',
    ];
	public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function cryptoWallets(): HasMany
    {
        return $this->hasMany(CryptoWallet::class);
    }

    public function socialMedias(): HasMany
    {
        return $this->hasMany(SocialMedia::class);
    }

    public function iso(): HasOne
    {
        return $this->hasOne(Iso::class);
    }

    public function outGoingMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function incomingMessages(): HasMany
    {
        return $this->hasMany(Message::class, 'recipient_id');
    }

    public function channelMessages() :HasMany
    {
        return $this->hasMany(ChannelMessage::class, 'sender_id');
    }

    public function channels() :BelongsToMany
    {
        return $this->belongsToMany(Channel::class)->withPivot(['is_owner'])->withTimestamps();
    }

    public function shares() :HasMany
    {
        return $this->hasMany(Share::class);
    }
    public static function getAllIso()
    {
        $iso = User::whereHas(
            'roles', function($q){
                $q->where('name', 'iso-initiator');
            }
        )->get();
        return $iso;
    }
}
