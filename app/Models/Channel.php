<?php

namespace App\Models;

use App\Enums\ChannelType;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Channel extends Model
{
    use HasFactory, HasUuid;

    protected $fillable = [
        'uuid',
        'user_id',
        'text',
        'type',
    ];

    protected $casts = [
        'type' => ChannelType::class,
    ];

    public function messages() :HasMany
    {
        return $this->hasMany(ChannelMessage::class);
    }

    public function users() :BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
