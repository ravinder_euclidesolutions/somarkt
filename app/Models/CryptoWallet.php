<?php

namespace App\Models;

use App\Enums\CryptoWalletType;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CryptoWallet extends Model {

    use HasFactory, HasUuid;

    protected $fillable = [
        'uuid',
        'address',
        'type',
        'is_active'
    ];

    protected $casts = [
        'type'      => CryptoWalletType::class,
        'is_active' => 'bool'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
