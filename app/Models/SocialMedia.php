<?php

namespace App\Models;

use App\Enums\SocialMediaType;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SocialMedia extends Model {

    use HasFactory, HasUuid;

    protected $fillable = [
        'uuid',
        'user_id',
        'name',
        'url',
        'is_approved'
    ];

    protected $casts = [
        'name'        => SocialMediaType::class,
        'is_approved' => 'bool'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
