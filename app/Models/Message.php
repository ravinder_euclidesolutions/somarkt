<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory, HasUuid;

    protected $fillable = [
        'uuid',
        'recipient_id',
        'sender_id',
        'text',
        'seen_at'
    ];

    protected $casts = [
        'seen_at' => 'datetime'
    ];
}
