<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Translatable\HasTranslations;

class Phase extends Model {

    use HasFactory, HasUuid, HasTranslations;

    protected $fillable = [
        'uuid',
        'name',
        'shareable_stock',
        'stock_price',
        'duration',
        'order'
    ];

    public $translatable = ['name'];

    public function iso(): BelongsToMany
    {
        return $this->belongsToMany(Iso::class);
    }

}
