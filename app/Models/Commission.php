<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Translatable\HasTranslations;

class Commission extends Model {

    use HasFactory, HasUuid, HasTranslations;

    protected $fillable = [
        'uuid',
        'name',
        'commission',
        'is_active'
    ];

    public $translatable = ['name'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active'           => 'boolean',
        'email_verified_at'   => 'datetime',
        'account_verified_at' => 'datetime',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new ActiveScope);
    }

    public function shares(): BelongsToMany
    {
        return $this->belongsToMany(Share::class);
    }
}
