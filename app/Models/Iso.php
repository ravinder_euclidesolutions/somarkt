<?php

namespace App\Models;

use App\Enums\IsoStatusType;
use App\Enums\PhaseStatusType;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Tags\HasTags;

class Iso extends Model {

    use HasFactory, HasUuid, HasTags;

    protected $table = 'iso';

    protected $fillable = [
        'uuid',
        'user_id',
        'phase_id',
        'name',
        'code',
        'description',
        'status',
        'is_active'
    ];

    protected $casts = [
        'status'     => IsoStatusType::class,
        'is_active'  => 'bool',
        'created_at' => 'datetime:m/d/Y',
        'updated_at' => 'datetime:m/d/Y',
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray(): array
    {
        $array = $this->toArray();

        return [
            'uuid'        => $array['uuid'],
            'name'        => $array['name'],
            'code'        => $array['code'],
        ];
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function phases(): BelongsToMany
    {
        return $this->belongsToMany(Phase::class)
            ->as('phase')
            ->withPivot(['status', 'shareable_stock', 'stock_price', 'expire_at'])
            ->withTimestamps();
    }

    public function activePhase(): Collection
    {
        return $this->phases()->wherePivot('status', PhaseStatusType::InProgress)->get();
    }

    public function isActive(Builder $query): Builder
    {
        return $query->where('is_active', true);
    }

    public function shouldBeSearchable()
    {
        return $this->is_active;
    }

}
