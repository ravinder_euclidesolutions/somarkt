<?php

namespace App\Models;

use App\Enums\ShareStatusType;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Share extends Model {

    use HasFactory, HasUuid;

    protected $fillable = [
        'uuid',
        'user_id',
        'iso_id',
        'phase_id',
        'amount',
        'price_per_stock',
        'total',
        'gross_total',
        'status'
    ];

    protected $casts = [
        'expire_at' => 'datetime',
        'status'    => ShareStatusType::class,
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function iso(): BelongsTo
    {
        return $this->belongsTo(Iso::class);
    }

    public function phase(): BelongsTo
    {
        return $this->belongsTo(Phase::class);
    }

    public function commissions() :BelongsToMany
    {
        return $this->belongsToMany(Commission::class);
    }

}
