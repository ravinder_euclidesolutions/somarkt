<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilePath extends Model
{
    protected $table = "file_paths";
    protected $guarded = [];
}