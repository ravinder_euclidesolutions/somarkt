<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaValue extends Model
{
    protected $guarded = [];

    
    public function getMetaValueAttribute($value)
    {
        return unserialize($value);
    }
}
