<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static InProgress()
 * @method static static Completed()
 * @method static static Refunded()
 */
final class ShareStatusType extends Enum implements LocalizedEnum
{
    const InProgress = 'in_progress';
    const Completed = 'completed';
    const Refunded = 'refunded';
}
