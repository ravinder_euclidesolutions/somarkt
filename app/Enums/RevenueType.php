<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static IsoHolder()
 * @method static static Company()
 */
final class RevenueType extends Enum implements LocalizedEnum
{
    const IsoHolder = 'iso_holder';
    const Company = 'company';
}
