<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static Public()
 * @method static static Private()
 */
final class ChannelType extends Enum implements LocalizedEnum
{
    const Public = 'public';
    const Private = 'private';
}
