<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static InProgress()
 * @method static static Completed()
 * @method static static Cancelled()
 * @method static static Waiting()
 */
final class PhaseStatusType extends Enum implements LocalizedEnum {

    const InProgress = 'in_progress';
    const Waiting = 'waiting';
    const Completed = 'completed';
    const Cancelled = 'cancelled';
}
