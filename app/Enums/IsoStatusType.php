<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static WaitingApproval()
 * @method static static Approved()
 * @method static static Denied()
 */
final class IsoStatusType extends Enum implements LocalizedEnum
{
    const WaitingApproval = 'waiting_approval';
    const Approved = 'approved';
    const Denied = 'denied';
}
