<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Facebook()
 * @method static static Instagram()
 * @method static static Linkedin()
 * @method static static Twitter()
 * @method static static Tiktok()
 * @method static static Snapchat()
 * @method static static Youtube()
 */
final class SocialMediaType extends Enum
{
    const Facebook = 'facebook';
    const Instagram = 'instagram';
    const Linkedin = 'linkedin';
    const Twitter = 'twitter';
    const Tiktok = 'tiktok';
    const Snapchat = 'snapchat';
    const Youtube = 'youtube';
}
