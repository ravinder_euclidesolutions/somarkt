<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Bitcoin()
 * @method static static Eth()
 */
final class CryptoWalletType extends Enum
{
    const Bitcoin = 'BTC';
    const Eth = 'ETH';
}
