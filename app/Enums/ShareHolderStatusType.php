<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static Completed()
 * @method static static Refunded()
 * @method static static OnHold()
 */
final class ShareHolderStatusType extends Enum implements LocalizedEnum
{
    const Completed = 'completed';
    const Refunded = 'refunded';
    const OnHold = 'on_hold';

}
