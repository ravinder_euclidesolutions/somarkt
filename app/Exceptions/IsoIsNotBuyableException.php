<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Throwable;

class IsoIsNotBuyableException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('You can not buy stock from this ISO', Response::HTTP_FORBIDDEN, $previous);
    }
}
