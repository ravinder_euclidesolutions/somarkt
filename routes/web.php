<?php
use Illuminate\Support\Facades\Route;


Route::get('/', 'DashboardController@index');

/********* FRONT ROUTES ***********/

// Auth Routes
Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::resource('/profile','ProfileController');
    Route::get('/iso/list','ISOController@list');
    Route::resource('/iso','ISOController');
   
});

Route::get('/privacy-policy','DashboardController@privacy');
Route::get('/terms','DashboardController@terms');
Route::get('/howto','DashboardController@howto');
Route::get('/contact','DashboardController@help');
Route::post('/contact','DashboardController@storeHelp');

/************** END OF FRONT ROUTES ******************/


/********** Admin Routes **********/

Route::prefix('admin')->middleware('auth')->group(function () {
   Route::get('/home', 'HomeController@index')->name('home');
   Route::resource('/iso','UserController');
});

/********** END OF ADMIN ROUTES ***************/



/***************** DEPLOYMENT ROUTES ********************/

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

//Clear Config cache:
Route::get('/run-migrate', function() {
    $exitCode = Artisan::call('migrate');
    return '<h1>Run migration successfully</h1>';
});
//Clear Config cache:
Route::get('/run-db-seed', function() {
    $exitCode = Artisan::call('db:seed');
    return '<h1>Run DB seeds successfully</h1>';
});

//Clear Config cache:
Route::get('/config-clear', function() {
    $exitCode = Artisan::call('config:clear');
    return '<h1>Clear Config cleared</h1>';
});