<?php

namespace Database\Seeders;

use App\Models\Commission;
use Illuminate\Database\Seeder;

class CommissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyCommission = new Commission();
        $companyCommission->name = ['en' => 'Company Commission', 'tr' => 'Şirket Komisyonu'];
        $companyCommission->commission = config('share.company_commission_rate') / 100;
        $companyCommission->is_active = true;
        $companyCommission->save();

        $isoCommission = new Commission();
        $isoCommission->name = ['en' => 'ISO Commission', 'tr' => 'ISO Komisyonu'];
        $isoCommission->commission = config('share.iso_initiator_commission_rate') / 100;
        $isoCommission->is_active = true;
        $isoCommission->save();

        $dividendCommission = new Commission();
        $dividendCommission->name = ['en' => 'Dividend', 'tr' => 'Kar Payı'];
        $dividendCommission->commission = config('share.dividend_commission_rate') / 100;
        $dividendCommission->is_active = true;
        $dividendCommission->save();
    }
}
