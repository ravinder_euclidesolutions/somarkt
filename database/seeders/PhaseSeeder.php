<?php

namespace Database\Seeders;

use App\Models\Phase;
use Illuminate\Database\Seeder;

class PhaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phaseOne = new Phase();
        $phaseOne->setTranslation('name', 'tr', 'Faz 1');
        $phaseOne->setTranslation('name', 'en', 'Phase 1');
        $phaseOne->shareable_stock = \config('share.phase_one_producible_ticker_count');
        $phaseOne->stock_price = \config('share.phase_one_ticker_price');
        $phaseOne->duration = \config('share.phase_one_duration_day');
        $phaseOne->order = 1;
        $phaseOne->save();

        $phaseTwo = new Phase();
        $phaseTwo->setTranslation('name', 'tr', 'Faz 2');
        $phaseTwo->setTranslation('name', 'en', 'Phase 2');
        $phaseTwo->shareable_stock = \config('share.phase_two_producible_ticker_count');
        $phaseTwo->stock_price = \config('share.phase_two_ticker_price');
        $phaseTwo->duration = \config('share.phase_two_duration_day');
        $phaseTwo->order = 2;
        $phaseTwo->save();

    }
}
