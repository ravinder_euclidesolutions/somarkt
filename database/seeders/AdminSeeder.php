<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'     => 'Super',
            'surname'  => 'Admin',
            'email'    => 'admin@admin.com',
            'phone'    => '+905442530614',
            'password' => 123456,
        ]);

        $user->assignRole('super-admin');
    }
}
