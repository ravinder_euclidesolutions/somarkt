<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenueableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenueable', function (Blueprint $table) {
            $table->unsignedBigInteger('revenue_id');
            $table->morphs('revenueable');
            $table->unique(['revenue_id', 'revenueable_id', 'revenueable_type']);

            $table->foreign('revenue_id')->references('id')->on('revenues')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenueable');
    }
}
