<?php

use App\Enums\PhaseStatusType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsoPhaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_phase', function (Blueprint $table) {
            $table->unsignedBigInteger('iso_id');
            $table->unsignedBigInteger('phase_id');
            $table->unsignedBigInteger('shareable_stock');
            $table->unsignedDecimal('stock_price');
            $table->timestamp('expire_at');
            $table->enum('status', PhaseStatusType::getValues())->default(PhaseStatusType::Waiting);
            $table->timestamps();

            $table->foreign('iso_id')->references('id')->on('iso')->cascadeOnDelete();
            $table->foreign('phase_id')->references('id')->on('phases')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iso_phase');
    }
}
