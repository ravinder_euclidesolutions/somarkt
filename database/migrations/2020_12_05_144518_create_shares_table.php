<?php

use App\Enums\ShareStateType;
use App\Enums\ShareStatusType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shares', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->index();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('iso_id');
            $table->unsignedBigInteger('phase_id');
            $table->unsignedBigInteger('amount');
            $table->unsignedDecimal('price_per_stock');
            $table->unsignedBigInteger('total');
            $table->unsignedDecimal('gross_total', 8, 3);
            $table->enum('status', [ShareStatusType::getValues()])->default(ShareStatusType::InProgress);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('iso_id')->references('id')->on('iso')->cascadeOnDelete();
            $table->foreign('phase_id')->references('id')->on('phases')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shares');
    }
}
