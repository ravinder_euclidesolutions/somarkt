@extends('layouts.app')

@section('content')
<div class="content custom-scrollbar ps">
        <div id="project-dashboard" class="doc forms-doc page-layout simple full-width">
            <!-- HEADER -->

            <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center ">
                @if($user->avatar == 'profile.jpg')
                <img class="profile-image avatar huge mr-6"  src="{{ asset('public/images/avatars/profile.jpg')}} ">
                @else
                <img class="profile-image avatar huge mr-6"  src="{{ asset('public/')}}/{{ $user->avatar }} ">
                @endif
                <div class="name h4 my-6">Edit Profile</div>
            </div>
            <!-- CONTENT -->
            <div class="page-content p-6">
                <div class="container">

                    <form name="customerRegisterForm" id="customerRegisterForm" data-toggle="validator" role="form" method="POST" action="{{ url('/profile')}}/{{$user->id }}"  enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        @method('PATCH')

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="user-tab-pane" role="tabpanel" aria-labelledby="user-tab">
                            <div class="widget-group row no-gutters">
                                <div class="widget widget1 card p-6 col-12">
                                    <div class="form-row">
                                        <div class="form-group col-md-6 required">
                                             <label for="name" class="col-form-label">Name <span class="text-danger">&#42;</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="inputfname" placeholder="eg. Mikey" value="{{ $user->name }}" required >

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="name" class="col-form-label">Surname</label>
                                            <input type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" id="inputsurname" placeholder="eg. Mikey" value="{{ $user->surname }}">

                                            @error('surname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                             <label for="phone" class="col-form-label">Phone</label>
                                            <input type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" id="inputfname" placeholder="eg. (555) 555-1234" value="{{ $user->phone }}"  >

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4" class="col-form-label">Email <span class="text-danger">&#42;</span></label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="inputEmail4" placeholder="example@gamil.com" value="{{ $user->email }}" required >
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group mb-4 col-6">
                                            <label for="registerFormInputPassword">Password</label>
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="registerFormInputPassword" name="password"placeholder="********************" />

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                        </div>

                                        <div class="form-group mb-4 col-6">
                                            <label for="registerFormInputPasswordConfirm">Password (Confirm) <span class="text-danger">*</span></label>
                                            <input type="password" class="form-control" id="registerFormInputPasswordConfirm"  name="password_confirmation" placeholder="********************" />
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6 required">
                                            <label for="ETH_address" class="col-form-label">ETH_address</label>
                                            <input type="tel" class="form-control @error('ETH_address') is-invalid @enderror" name="ETH_address" id="inputETH_address" placeholder="564654654WERE" value="{{ $user->ETH_address }}"  >
                                            @error('ETH_address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group  col-md-6">
                                        <label for="avatar">Upload Profile Pic</label>
                                        <input type="file" class="form-control @error('avatar') is-invalid @enderror"  name="avatar" id="customFile">
                                        @error('avatar')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <button type="submit" class="btn btn-primary mt-5">Update Profile</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- / CONTENT -->
    </div>


<script type="text/javascript">
    // date and time field
    $(".form_datetime").datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0
    });

    $(document).ready(function(){

        $("#btnResetPassword").click(function (e) {
            e.preventDefault();
            $(".se-pre-con").show();
            $.ajax({
               url: '{{ url('users/password/reset') }}/'+{{ $user->id }},
               type:"get" ,
               success: function(response) {

                $(".se-pre-con").fadeOut("slow");

                if(response == 'ok') {
                   toastr.success('Password Changed Successfully', 'Success', {
                        timeOut:2000,
                        onHidden: function () {
                            window.location.href = "{{ url('users') }}";
                     } });
                }

               }
            });

        });
    });


</script>

@endsection

