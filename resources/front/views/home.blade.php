@extends('layouts.app')

@section('content')

<div class="content custom-scrollbar ps">
    <div id="project-dashboard" class="page-layout simple right-sidebar">
        <div class="page-content-wrapper custom-scrollbar ps ps--active-x ps--active-y">
            <!-- CONTENT -->
            <div class="page-content">
                @auth
                <div class="marquee-box d-flex border-bottom">
                    <div class="col-2 p-3 bg-secondary text-white">My Stock</div>
                    <marquee behavior="scroll" class="p-3" direction="left">
                        @foreach($users as $user)
                        <span class="m-2 ">{{ $user->name }}</span>
                        @endforeach
                    </marquee>
                </div>
                @endauth
                <div class="marquee-box d-flex border-bottom">
                    <div class="col-2 p-3 bg-secondary text-white">SoMarkt</div>
                    <marquee behavior="scroll" class="p-3" direction="left">
                        @foreach($users as $user)
                        <span class="m-2 ">{{ $user->name }}</span>
                        @endforeach
                    </marquee>

                </div>
                <ul class="nav nav-tabs bg-secondary" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link btn active fuse-ripple-ready text-white" id="home-tab" data-toggle="tab" href="#home-tab-pane" role="tab" aria-controls="home-tab-pane" aria-expanded="true">ISO's</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn fuse-ripple-ready text-white" id="budget-summary-tab" data-toggle="tab" href="#budget-summary-tab-pane" role="tab" aria-controls="budget-summary-tab-pane">BUY</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn fuse-ripple-ready text-white" id="team-members-tab" data-toggle="tab" href="#team-members-tab-pane" role="tab" aria-controls="team-members-tab-pane">SELL</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active p-6" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab">
                      <!-- WIDGET GROUP -->
                        <div class="widget-group row no-gutters">
                        @foreach($users as $key => $user)
                            <div class="card col-2">
                                @if($user->avatar == 'profile.jpg')
                                <img class="card-img-top"  src="{{ asset('public/images/avatars/profile.jpg')}} ">
                                @else
                                <img class="card-img-top"  src="{{ asset('public/')}}/{{ $user->avatar }} ">
                                @endif
                                <div class="card-body text-center">
                                    <h6 class="">{{$user->name}}</h6>
                                    <p class="card-text">875122 Sold</p>
                                    <p class="card-text">10 days left</p>
                                </div>
                            </div>
                        @endforeach

                        </div>
                        <!-- / WIDGET GROUP -->
                    </div>
                    <div class="tab-pane fade p-6" id="budget-summary-tab-pane" role="tabpanel" aria-labelledby="budget-summary-tab">

                        <!-- WIDGET GROUP -->
                        <div class="widget-group row no-gutters">
                        @foreach($users as $key => $user)
                            <div class="card col-2">
                                @if($user->avatar == 'profile.jpg')
                                <img class="card-img-top"  src="{{ asset('public/images/avatars/profile.jpg')}} ">
                                @else
                                <img class="card-img-top"  src="{{ asset('public/')}}/{{ $user->avatar }} ">
                                @endif
                                <div class="card-body text-center">
                                    <h6 class="">{{$user->name}}</h6>
                                    <p class="card-text">875122 Sold</p>
                                    <p class="card-text">10 days left</p>
                                </div>
                            </div>
                        @endforeach
                        </div>
                        <!-- / WIDGET GROUP -->
                    </div>
                    <div class="tab-pane fade p-6" id="team-members-tab-pane" role="tabpanel" aria-labelledby="team-members-tab">

                        <!-- WIDGET GROUP -->
                        <div class="widget-group row no-gutters">
                        @foreach($users as $key => $user)
                            <div class="card col-2">
                                @if($user->avatar == 'profile.jpg')
                                <img class="card-img-top"  src="{{ asset('public/images/avatars/profile.jpg')}} ">
                                @else
                                <img class="card-img-top"  src="{{ asset('public/')}}/{{ $user->avatar }} ">
                                @endif
                                <div class="card-body text-center">
                                    <h6 class="">{{$user->name}}</h6>
                                    <p class="card-text">875122 Sold</p>
                                    <p class="card-text">10 days left</p>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- / CONTENT -->
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#customer-data-table').DataTable({
        "searching": true,
        "ordering": true,
        "bPaginate": true,
        "bInfo": false,
        "lengthChange": false,
        "pageLength": 10,
        language: {
            searchPlaceholder: "Search records here",
        }
    });

    $('#customer-sale-table').DataTable({
        "searching": true,
        "ordering": true,
        "bPaginate": true,
        "bInfo": false,
        "lengthChange": false,
        "pageLength": 10,
        language: {
            searchPlaceholder: "Search records here",
        }
    });

    $('#customer-buy-table').DataTable({
        "searching": true,
        "ordering": true,
        "bPaginate": true,
        "bInfo": false,
        "lengthChange": false,
        "pageLength": 10,
        language: {
            searchPlaceholder: "Search records here",
        }
    });
</script>



@endsection
