@extends('layouts.app')

@section('content')
<div class="content custom-scrollbar ps">
    <div class="doc forms-doc page-layout simple full-width">
        <!-- CONTENT -->
        <div class="page-content p-6">
            <div class="content container">
                <div class="row">
                <div class="col-12">
                    <div class="example">
                        <div class="description">
                            <div class="description-text">
                                <h5>Contact Us</h5>
                            </div>
                        </div>

                        @if (Session::has('success'))
                        <script type="text/javascript">
                            $(document).ready(function(){
                                PNotify.success({
                                title: 'Success',
                                text: 'Submitted Request Successfully!',
                                animation: 'slide',
                                delay: 3000,
                                });
                            });
                        </script>
                        @endif

                        <div class="source-preview-wrapper">
                            <div class="preview">
                                <div class="preview-elements">
                                    <form name="contactForm" data-toggle="validator"  id="contactForm" method="POST" action="{{ url('contact') }}">
                                        @csrf
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Name<span class="text-danger">*</span></label>
                                                <input class="form-control form_datetime @error('name') is-invalid @enderror" type="text" name="name"  placeholder="eg: Jhon"    required/>

                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="example-date-input">Email <span class="text-danger">*</span></label>
                                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="registerFormInputEmail" aria-describedby="emailHelp" required  name="email" value="{{ old('email') }}"   autocomplete="email" placeholder="example@gmail.com" />
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-row">

                                            <div class="form-group col-md-12">
                                                <label  for="payroll_details">Message <span class="text-danger">*</span></label>
                                                <textarea required class="form-control @error('message') is-invalid @enderror" id="message" rows="5" name="message"  ></textarea>

                                                @error('message')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>





@endsection
