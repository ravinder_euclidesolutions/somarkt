@extends('layouts.app')

@section('content')
 
         



<div class="content custom-scrollbar ps">
    <div id="project-dashboard" class="page-layout simple right-sidebar">
        <div class="page-content-wrapper custom-scrollbar ps ps--active-x ps--active-y">
            <!-- CONTENT -->
            <div class="page-content p-6">
            <div class="description">
                                    <div class="description-text row col-12">
                                        <div class="col-6">
                                        <h2 class="" id="striped-rows">My ISO List</h2> 
                                        </div>                                    
                                    </div>                                               
                                </div>
                <div class="tab-content">
                    
                        <!-- WIDGET GROUP -->
                        <div class="widget-group row no-gutters">
                        <div class="table-responsive"> 
                                                <table id="customer-data-table" class="table table-striped dataTable no-footer" role="grid">
                                                    <thead>
                                                        <tr>
                                                            <th class="secondary-text">
                                                                <div class="table-header">
                                                                    <span class="column-title">Name</span>
                                                                </div>
                                                            </th>
                                                            <th class="secondary-text">
                                                                <div class="table-header">
                                                                    <span class="column-title">Code</span>
                                                                </div>
                                                            </th> 
                                                            <th class="secondary-text">
                                                                <div class="table-header">
                                                                    <span class="column-title">Description</span>
                                                                </div>
                                                            </th>
                                                            <th class="secondary-text">
                                                                <div class="table-header">
                                                                    <span class="column-title">Action</span>
                                                                </div>
                                                            </th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($list as $key => $iso)        
                                                        <tr>
                                                            <td>{{ $iso->name }}</td>
                                                            <td>{{ $iso->code }}</td>                    
                                                            <td><?php echo  substr($iso->description,0,100); ?></td> 
                                                            <td>
                                                                <input type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger">
                                                            </td>  
                                                        </tr>                                                      
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <script type="text/javascript">
                                                $('#customer-data-table').DataTable({
                                                    "searching": true,
                                                    "ordering": true,
                                                    "bPaginate": true,
                                                    "bInfo": false,
                                                    "lengthChange": false,
                                                    "pageLength": 10,
                                                    language: {
                                                        searchPlaceholder: "Search records here",
                                                    }
                                                });
                                            </script>
                        </div>
                        <!-- / WIDGET GROUP -->
                   
                </div>
            </div>
            <!-- / CONTENT -->
        </div>
    </div>
</div>
<style type="text/css">
    #add-user-button {
        position: absolute;
        right: 2.4rem;
        bottom: 12px;
        z-index: 99;
    }
</style>

 
@endsection