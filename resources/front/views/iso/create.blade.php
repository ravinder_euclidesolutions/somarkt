@extends('layouts.app')

@section('content')
<div class="content custom-scrollbar ps">
    <div class="doc forms-doc page-layout simple full-width">
        
        <!-- CONTENT -->
        <div class="page-content p-6">
            <div class="content container">
            @if(isset($message))<div class="row">
                <div class="col-12">
                <div class="status-message">{{$message}}</div>
                </div></div>@endif
                <div class="row">
                <div class="col-12">
                    <div class="example">
                        <div class="description">
                            <div class="description-text">
                                <h5>Create ISO</h5>
                            </div>
                        </div>
                        <div class="source-preview-wrapper">
                            <div class="preview">
                                <div class="preview-elements">
                                    <form name="contactForm" data-toggle="validator" role="form" method="POST" action="{{ url('/iso') }}" autocomplete="off">
                                        @csrf
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Name<span class="text-danger">*</span></label>
                                                <input class="form-control form_datetime @error('name') is-invalid @enderror" type="text" name="name"  placeholder="eg: Jhon" required  />

                                                @error('payrollrequest')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="example-date-input">Code <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control @error('code') is-invalid @enderror" id="registerFormInputCode"   name="code" value="{{ old('email') }}" required   placeholder="iso code" />
                                                @error('code')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-row">

                                            <div class="form-group col-md-12">
                                                <label  for="payroll_details">Description</label>
                                                <textarea class="form-control @error('description') is-invalid @enderror" id="description" rows="5" name="description"></textarea>

                                                @error('description')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>





@endsection
