@extends('layouts.app')

@section('content')
<div class="content custom-scrollbar ps">
    <div id="register" class="p-8">

        <div class="form-wrapper md-elevation-8 p-8">


            <div class="title mt-4 mb-8">Create an account</div>

            <form name="registerForm" method="POST" action="{{ route('register') }}">
                @csrf
                <div class="row">
                    <div class="form-group mb-4 col-6">
                        <label for="registerFormInputName">Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="registerFormInputName" aria-describedby="nameHelp" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter Name" />

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mb-4 col-6">
                        <label for="registerFormInputName">Surname</label>
                        <input type="text" class="form-control @error('surname') is-invalid @enderror" id="registerFormInputSurname" aria-describedby="nameHelp" name="surname" value="{{ old('surname') }}" autofocus placeholder="Enter Surname" />

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mb-4 col-6">
                        <label for="registerFormInputEmail">Email address <span class="text-danger">*</span></label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="registerFormInputEmail" aria-describedby="emailHelp"  name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="example@gmail.com" />
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mb-4 col-6">
                        <label for="registerFormInputPhone">Phone</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="registerFormInputPhone"  name="phone" value="{{ old('phone') }}" placeholder="Enter phone number" />
                        @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                </div>
               <div class="row">
                    <div class="form-group mb-4 col-6">
                        <label for="security_code">Enter Security Code <span class="text-danger">*</span></label>
                        <input type="text" class="form-control @error('security_code') is-invalid @enderror" id="security_code" name="security_code" required   placeholder="Enter security answer" />
                        @error('security_code')
                            <span class="invalid-feedback" role="alert">
                                <strong>Your Security Answer Does not match</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row" id="password_fields" style="display:none;">
                    <div class="form-group mb-4 col-6">
                            <label for="registerFormInputPassword">Password <span class="text-danger">*</span></label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="registerFormInputPassword" name="password" required autocomplete="new-password" placeholder="Enter password" />

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <div class="form-group mb-4 col-6">
                        <label for="registerFormInputPasswordConfirm">Password (Confirm) <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" id="registerFormInputPasswordConfirm"  name="password_confirmation" required autocomplete="new-password" placeholder="Confirm your password" />
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mb-4 col-6">
                        <label for="ETH_address">Ethereum Wallet Address</label>
                        <input type="text" class="form-control" id="eth"  name="ETH_address"  placeholder="ETH Wallet Address" />
                    </div>
                    <div class="form-group mb-4 col-6">
                        <label for="code">ISO Code</label>
                        <input type="text" class="form-control" id="code"  name="code"  placeholder="Enter Unique Code" />
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mb-4 col-12">
                        <label for="tags">Short Description</label>
                        <input type="text" class="form-control" id="description"  name="description"  placeholder="Enter short description" />
                    </div>
                    
                </div>
                <div class="row">
                    <div class="form-group mb-4 col-6">
                        <label for="tags">Tags</label>
                        <input type="text" class="form-control" id="tags"  name="tags"  placeholder="Enter tags (Separate by Comma)" />
                    </div>
                    
                </div>
                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN" disabled>
                    CREATE MY ACCOUNT
                </button>

            </form>

            <div class="login d-flex flex-column flex-sm-row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                <span class="text mr-sm-2">Already have an account?</span>
                <a class="link text-secondary" href="{{ route('login') }}">Log in</a>
            </div>

        </div>
    </div>

</div>
<script>
    $('#security_code').keyup(function() {
        var security = $(this).val();
        console.log(security);
        if(security == 'somarkt_2021') {
            $('#password_fields').show();
            $('.submit-button').attr('disabled',false);
        } else {
            $('#password_fields').hide();
            $('.submit-button').attr('disabled',true);
        }
    });


</script>
@endsection
