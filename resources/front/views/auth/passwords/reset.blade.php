@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="content custom-scrollbar">
 
        <div id="forgot-password" class="p-8">
 
            <div class="form-wrapper md-elevation-8 p-8">

                 <img  src="{{ asset('public/images/logos/somarktLogo.svg')}} " alt="logo" style="width: 150px;">
                 
                <div class="title mt-4 mb-8">Recover your password</div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <form name="forgotPasswordForm" novalidate  method="POST" action="{{ route('password.update') }}">
                    @csrf
                   <input type="hidden" name="token" value="{{ $token }}">
                   
                    <div class="form-group mb-4">
                        <label for="resetPasswordFormInputEmail">Email address</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                        
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>    
                    <div class="form-group mb-4">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        <label for="registerFormInputPassword">Password</label>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="form-group mb-4">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        <label for="registerFormInputPasswordConfirm">Password (Confirm)</label>
                    </div>
                                
                    <button type="submit" class="submit-button btn btn-block btn-secondary mt-8 mb-4 mx-auto" aria-label="RESET MY PASSWORD">
                        RESET MY PASSWORD
                    </button>

                </form>

                <div class="login row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                    <a class="link text-secondary" href="{{ route('login') }}">Go back to login</a>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection
