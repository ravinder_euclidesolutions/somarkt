@extends('layouts.app')

@section('content')
<div class="content custom-scrollbar ps">

    <div id="login" class="p-8">

        <div class="form-wrapper md-elevation-8 p-8">

            <img  src="{{ asset('public/images/logos/somarktLogo.svg')}} " alt="logo" style="width: 150px;">

            <div class="title mt-4 mb-8">Login to your account</div>

            <form name="loginForm" novalidate method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group mb-4">
                    <label for="loginFormInputEmail">Email address <span class="text-danger">*</span></label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="loginFormInputEmail" aria-describedby="emailHelp" placeholder="Your email address " />

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group mb-4">
                    <label for="loginFormInputPassword">Password <span class="text-danger">*</span></label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="loginFormInputPassword" placeholder="Your password" />
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="remember-forgot-password row no-gutters align-items-center justify-content-between pt-4">

                    <div class="form-check remember-me mb-4">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input " aria-label="Remember Me" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                            <span class="checkbox-icon"></span>
                            <span class="form-check-description">Remember Me</span>
                        </label>
                    </div>

                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}" class="forgot-password text-secondary mb-4">Forgot Password?</a>
                </div>
                @endif
                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                    LOG IN
                </button>

                <div class="login d-flex flex-column flex-sm-row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                    <span class="text mr-sm-2">New to SoMarkt</span>
                    <a class="link text-secondary" href="{{ route('register') }}">Register</a>
                </div>

            </form>



        </div>
    </div>

</div>

@endsection
