@extends('layouts.app')
 
@section('content')
<div class="content-wrapper">
    <div class="content custom-scrollbar">  
        <div id="project-dashboard" class="doc forms-doc page-layout simple full-width"> 
            <!-- HEADER -->
            <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">   <h1 class="doc-title h4" id="content">Team</h1>                
            </div> 
            <!-- CONTENT -->
            <div class="page-content p-6">
                <div class="container">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link btn active" id="user-tab" data-toggle="tab" href="#user-tab-pane" role="tab" aria-controls="user-tab-pane" aria-expanded="true">User Profile</a>
                        </li>
                        <li class="nav-item" id="client-access">
                            <a class="nav-link btn" id="client-tab" data-toggle="tab" href="#client-tab-pane" role="tab" aria-controls="client-tab-pane" aria-expanded="true">Client Access</a>
                        </li>
                    </ul>

                    <form name="customerRegisterForm" id="customerRegisterForm" data-toggle="validator" role="form" method="POST" action="{{ url('/users') }}"  enctype="multipart/form-data" autocomplete="off">
                        @csrf
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="user-tab-pane" role="tabpanel" aria-labelledby="user-tab">
                            <div class="widget-group row no-gutters">                              
                                <div class="widget widget1 card p-6 col-12">  
                                    <div class="form-row">
                                        <div class="form-group col-md-6 required">
                                             <label for="name" class="col-form-label">Name <span class="text-danger">&#42;</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="inputfname" placeholder="eg. fixe" value="{{ old('name') }}" required autocomplete="off">
                                           
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div> 
                                        <div class="form-group col-md-6">
                                            <label>User Role</label>
                                            <select class="form-control " name="userType" id="role" required> 
                                                <option value="" selected disabled>Select Role</option> 
                                                @role('Super Admin')      
                                                <option value="Super Admin">Super Admin</option> 
                                                @endrole    
                                                <option value="Admin">Admin</option>                  
                                            </select>   
                                        </div>                                      
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4" class="col-form-label">Email <span class="text-danger">&#42;</span></label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="inputEmail4" placeholder="example@gamil.com" value="{{ old('email') }}" required >                                                    
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6 required">
                                             <label for="phone" class="col-form-label">Phone</label>
                                            <input type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" id="inputfname" placeholder="eg. (555) 555-1234" value="{{ old('phone') }}"  >
                                           
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>                                      
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="dob" class="col-form-label">Date Of Birth</label>
                                            <input size="16" type="text" class="form_datetime form-control" id="dob" name="dob" placeholder="2021-12-24" autocomplete="off"> 
                                            @error('dob')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group  col-md-6">
                                            <label for="avatar">Profile Image</label>
                                            <input type="file" class="form-control @error('avatar') is-invalid @enderror"  name="avatar" id="customFile">
                                            @error('avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div> 
                                    </div>       
                                     
                                </div>                              
                            </div>
                        </div>
                        <div class="tab-pane fade " id="client-tab-pane" role="tabpanel" aria-labelledby="client-tab">                          
                            <div class="widget-group row no-gutters">                                
                                <div class="widget widget1 card col-12">   
                                    <table class="table table-hover" id="clientlist">
                                        <thead>
                                            <tr>
                                                <th>Select Clients</th>                            
                                                <th>Name</th> 
                                            </tr>
                                        </thead>
                                        <tbody>                                                       
                                            @foreach($clients as $client)
                                             <tr>
                                                <td>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" value="{{ $client->id }}" name="clients[]">
                                                        <span class="custom-control-indicator fuse-ripple-ready"></span>
                                                    </label>
                                                </td>                                                             
                                                <td>{{ $client->name }}</td>                                
                                            </tr>
                                            @endforeach                                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 
                            <button type="submit" class="btn btn-primary mt-5">Create New</button>  
                        </form> 
                    </div>
                </div>
            </div> 
        </div>
    <!-- / CONTENT -->
    </div>     
</div> 

<script type="text/javascript">     
    // date and time field
    $(".form_datetime").datepicker({            
        dateFormat: 'yy-mm-dd',  
        maxDate: 0           
    });  

    $('#role').change(function(){    

        var role = $(this).val();
        if(role == 'Super Admin') {
            $('#client-tab').addClass("d-none");           
        } else {
           $('#client-tab').removeClass("d-none");           
        }    
    });  
</script>
  
@endsection
 
 