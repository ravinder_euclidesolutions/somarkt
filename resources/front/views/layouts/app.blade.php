<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  @include('layouts.head')

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body class="layout layout-vertical layout-left-navigation layout-below-toolbar layout-below-footer media-step-xl fuse-aside-expanded">
    <main>

        <div id="wrapper">

            @include('layouts.sidebar')

            <div class="content-wrapper">

                @include('layouts.header')

                @section('content')
                @show

            </div>

    @include('layouts.footer')

