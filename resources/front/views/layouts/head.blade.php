
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">

    <title>{{ config('app.name', 'SoMarkt') }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/images/logos/favicon.ico') }}" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">

    <!-- STYLESHEETS -->
    <style type="text/css">
        [fuse-cloak],
        .fuse-cloak {
            display: none !important;
        }
    </style>

    <!-- Icons.css -->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/icons/fuse-icon-font/style.css') }}" >

    <!-- PNotify -->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/node_modules/pnotify/dist/PNotifyBrightTheme.css') }} ">

    <!-- Perfect Scrollbar -->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/node_modules/perfect-scrollbar/css/perfect-scrollbar.css' ) }}"/>

    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/main.css') }} ">
    <!-- / STYLESHEETS -->

    <!-- Mobile Detect -->
    <script type="text/javascript" src="{{ asset('public/node_modules/mobile-detect/mobile-detect.min.js') }}"></script>

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('public/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

     <!-- Popper.js -->
    <script type="text/javascript" src="{{ asset('public/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>

     <!-- Bootstrap -->
     <script type="text/javascript" src="{{ asset('public/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Perfect Scrollbar -->
    <script type="text/javascript" src="{{ asset('public/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>


    <!-- Data tables -->
    <script type="text/javascript" src="{{ asset('public/node_modules/datatables.net/js/jquery.dataTables.js') }}"></script>

    <!-- PNotify -->
    <script type="text/javascript" src="{{ asset('public/node_modules/pnotify/dist/iife/PNotify.js') }} "></script>
    <script type="text/javascript" src="{{ asset('public/node_modules/pnotify/dist/iife/PNotifyStyleMaterial.js') }} "></script>
     <!-- Main JS -->
    <script type="text/javascript" src="{{ asset('public/js/main.js') }}"></script>

    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>

    <!-- Calander assets -->
    <link rel="stylesheet" href="{{ asset('public/css/fullcalendar.css') }}" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="{{ asset('public/js/moment.min.js') }}"></script>
    <script src="{{ asset('public/js/fullcalendar.min.js') }}"></script>

     <!-- Date and time Picker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

    <!-- Custom CSS-->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/custom.css') }} ">

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
