<nav id="toolbar" class="bg-white">

    <div class="col-auto">

        <div class="row no-gutters align-items-center justify-content-end">
        @auth
            <div class="user-menu-button dropdown">

                <div class="dropdown-toggle ripple row align-items-center no-gutters px-2 px-sm-4 fuse-ripple-ready" role="button" id="dropdownUserMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="avatar-wrapper">
                        @if(auth()->user()->avatar == 'profile.jpg')
                        <img class="avatar"  src="{{ asset('public/images/avatars/profile.jpg')}} ">
                        @else
                        <img class="avatar"  src="{{ asset('public/')}}/{{ auth()->user()->avatar }} ">
                        @endif
                        <i class="status text-green icon-checkbox-marked-circle s-4"></i>
                    </div>
                    <span class="username mx-3 d-none d-md-block">{{ auth()->user()->name }}</span>
                </div>

                <div class="dropdown-menu" aria-labelledby="dropdownUserMenu">

                    <a class="dropdown-item fuse-ripple-ready" href="{{ url('/profile') }}/{{ $hashids->encode(auth()->user()->id) }}">
                        <div class="row no-gutters align-items-center flex-nowrap">
                            <i class="icon-account"></i>
                            <span class="px-3">My Profile</span>
                        </div>
                    </a>

                    <a class="dropdown-item fuse-ripple-ready" href="{{ url('/iso/list') }}">
                        <div class="row no-gutters align-items-center flex-nowrap">
                            <i class="status icon-tile-four"></i>
                            <span class="px-3">My ISO List</span>
                        </div>
                    </a>
                    <a class="dropdown-item fuse-ripple-ready" href="{{ url('/iso/create') }}">
                        <div class="row no-gutters align-items-center flex-nowrap">
                            <i class="status icon-book-open"></i>
                            <span class="px-3">Create ISO</span>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="nav-link ripple " href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="icon-logout s-4"></i>
                        <span>Logout</span>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                        </form>
                    </a>

                </div>
            </div>

        @else
            <a class="nav-link ripple " href="{{ route('login') }}" >
                Login
            </a>
            <div class="dropdown-divider"></div>
            <a class="nav-link ripple " href="{{ route('register') }}" >
                Register
            </a>
        @endauth
        </div>
    </div>

</nav>
