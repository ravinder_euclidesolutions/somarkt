<aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
    <div class="aside-content bg-white text-auto">

        <div class="aside-toolbar">
            <div class="logo">
                <img src="{{ asset('public/images/logos/somarktLogo.svg')}} " alt="logo" style="width:40px">
                <span class="ml-2">SoMarkt</span>
            </div>
            <button id="toggle-fold-aside-button" type="button" class="btn btn-icon d-none d-lg-block fuse-ripple-ready" data-fuse-aside-toggle-fold="">
                <i class="icon icon-backburger"></i>
            </button>
        </div>

        <ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">

            <li class="nav-item" role="tab" id="heading-dashboards">
                <a class="nav-link ripple {{ Request::path() == '/' ? 'active' : '' }} " href="{{ url('/') }}"  >
                    <i class="icon s-4 icon-tile-four"></i>
                    <span>Home</span>
                </a>
            </li>
            <li class="nav-item" role="tab" id="heading-market">
                <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-market" href="#" aria-expanded="false" aria-controls="collapse-users">
                    <i class="icon icon-chart-areaspline s-4"></i>
                    <span>Market</span>
                </a>
                <ul id="collapse-market" class='collapse ' role="tabpanel" aria-labelledby="heading-market" data-children=".nav-item">
                    <li class="nav-item">
                        <a class="nav-link ripple" href="#"  >
                            <i class="icon-view-list"></i>
                            <span>View All</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link ripple with-arrow collapsed {{ Request::is('contact') ? 'active' : '' }}" href="{{ url('/contact') }}">
                    <i class="icon icon-account-box s-4"></i>
                    <span>Contact Us</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link ripple with-arrow collapsed {{ Request::is('howto') ? 'active' : '' }}" href="{{ url('/howto') }}">
                    <i class="icon icon-book-open s-4"></i>
                    <span>How TO</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link ripple with-arrow collapsed {{ Request::is('terms') ? 'active' : '' }}" href="{{ url('/terms') }}">
                    <i class="icon icon-book-open s-4"></i>
                    <span>Terms of Use</span>
                </a>
            </li>
            <li class="nav-item">
            <a class="nav-link ripple with-arrow collapsed {{ Request::is('privacy-policy') ? 'active' : '' }}" href="{{ url('/privacy-policy') }}" >
                    <i class="icon icon-book-open-page-variant s-4"></i>
                    <span>Privacy</span>
                </a>
            </li>

        </ul>
    </div>

</aside>


