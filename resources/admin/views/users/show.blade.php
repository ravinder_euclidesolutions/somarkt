@extends('layouts.app')
 
@section('content') 
<div class="content-wrapper">
    <div class="content custom-scrollbar">
        <div  class="page-layout simple tabbed">
 
            <!-- HEADER -->
            <div class="page-header bg-primary light-fg d-flex flex-column justify-content-center justify-content-lg-end p-6">

                <div class="flex-column row flex-lg-row align-items-center align-items-lg-end no-gutters justify-content-between">

                    <div class="user-info flex-column row flex-lg-row no-gutters align-items-center">
 
                        @if($user->avatar != 'profile.jpg')
                          <img class="profile-image avatar huge mr-6 " src="{{$fileurl}}">
                        @else
                         <img class="profile-image avatar huge mr-6"  src="{{ asset('public/images/avatars/profile.jpg')}} ">
                        @endif

                        <div class="name h2 my-6">{{ $user->name }}</div>

                    </div>  
                    @role('Super Admin')
                    <div class="actions row align-items-center no-gutters">
                        <a href="{{ url('users') }}/{{ $hashids->encode($user->id) }}/edit" class="btn btn-secondary ml-2" aria-label="Edit"><i class="icon-lead-pencil"></i> Edit Info</a>                        
                    </div>
                    @endrole
                </div>
            </div>
            <!-- / HEADER -->

            <!-- CONTENT -->
            <div class="page-content">

                <ul class="nav nav-tabs" id="myTab" role="tablist"> 

                    <li class="nav-item">
                        <a class="nav-link btn active" id="about-tab" data-toggle="tab" href="#about-tab-pane" role="tab" aria-controls="about-tab-pane">About</a>
                    </li>       

                </ul>

                <div class="tab-content">

                  
                    <div class="tab-pane fade  show active" id="about-tab-pane" role="tabpanel" aria-labelledby="about-tab">

                        <div class="row">

                            <div class="about col-12 col-md-7 col-xl-9">

                                <div class="profile-box info-box general card mb-4">

                                    <header class="h6 bg-secondary text-auto p-4">
                                        <div class="title">General Information</div>
                                    </header>

                                    <div class="content p-4">
                                        <div class="info-line mb-6">
                                            <table class="table">
                                                 <tr class="job">
                                                    <td class="company font-weight-bold pr-4">Name</td>
                                                    <td class="date">{{ $user->name }}</td>
                                                </tr>
                                                <tr  class="job"> 
                                                    <td class="company font-weight-bold pr-4">Email</td>
                                                    <td class="date"> {{ $user->email }}</td>
                                                </tr>
                                            </table>
                                        </div> 

                                    </div>
                                </div> 

                            </div>
 
                        </div>
                    </div>

                    

                </div>
            </div>
            <!-- / CONTENT -->
        </div>

    </div>
</div>
       
	 
	 										         

@endsection