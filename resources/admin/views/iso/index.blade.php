@extends('layouts.app')
 
@section('content') 
<div class="content-wrapper">
    <div class="content custom-scrollbar">
        <div class="doc simple-table-doc page-layout simple full-width">
        <!-- HEADER -->
        <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
            <h1 class="doc-title h4" id="content">ISO</h1>   
        </div>                          
        <!-- / HEADER --> 
        <!-- CONTENT -->
        <div class="page-content p-6 pt-12">
            <div class="content container">
                <div class="row"> 

                    <div class="col-12">
                        <div class="example ">

                            <div class="description">
                                <div class="description-text row col-12">
                                    <div class="col-6">
                                       <h2 class="" id="striped-rows">ISO List</h2> 
                                    </div>                                    
                                </div>                                               
                            </div>

                           <div class="source-preview-wrapper">
                                <div class="preview">
                                    <div class="preview-elements">
                                    <div class="table-responsive"> 
                                        <table id="customer-data-table" class="table table-sm">
                                            <thead>
                                                <tr>
                                                    <th class="secondary-text">
                                                        <div class="table-header">
                                                            <span class="column-title">Name</span>
                                                        </div>
                                                    </th>
                                                    <th class="secondary-text">
                                                        <div class="table-header">
                                                            <span class="column-title">Email</span>
                                                        </div>
                                                    </th>  
                                                    <th class="secondary-text">
                                                        <div class="table-header">
                                                            <span class="column-title">Phone</span>
                                                        </div>
                                                    </th>   
                                                    <th class="secondary-text">
                                                        <div class="table-header">
                                                            <span class="column-title">ETH Address</span>
                                                        </div>
                                                    </th> 
                                                     
                                                    <th class="secondary-text">
                                                        <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                        </div>
                                                    </th>
                                                     
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $key => $user)        
                                                <tr>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>                    
                                                    <td>{{ $user->phone }}</td>                    
                                                    <td>{{ $user->ETH_address }}</td>                    
                                                    <td>
                                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger">
                                                    </td>  
                                                </tr>                                                      
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                        <script type="text/javascript">
                                            $('#customer-data-table').DataTable({
                                                "searching": true,
                                                "ordering": false,
                                                "bPaginate": true,
                                                "bInfo": false,
                                                "lengthChange": false,
                                                "pageLength": 10,
                                                language: {
                                                    searchPlaceholder: "Search records here",
                                                }
                                            });
                                        </script>

                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT -->
        </div>
        </div>
    </div>            
</div>   
                                                     
<style type="text/css">
    #add-user-button {
        position: absolute;
        right: 2.4rem;
        bottom: 12px;
        z-index: 99;
    }
</style>

 
@endsection