@extends('layouts.app')
 
@section('content')
<div class="content-wrapper">
    <div class="content custom-scrollbar">

        <div id="project-dashboard" class="page-layout simple right-sidebar">

            <div class="page-content-wrapper custom-scrollbar">

                <!-- HEADER -->
                <div class="page-header bg-primary text-auto d-flex flex-column justify-content-between px-6 pt-4 pb-0">

                    <div class="row no-gutters align-items-start justify-content-between flex-nowrap">

                        <div> 
                            <span class="h2">Welcome back, {{ Auth::user()->name }} !</span>  
                        </div> 

                        <button type="button" class="sidebar-toggle-button btn btn-icon d-block d-xl-none" data-fuse-bar-toggle="dashboard-project-sidebar" aria-label="Toggle sidebar">
                            <i class="icon icon-menu"></i>
                        </button>
                    </div>  
                </div>
                <!-- / HEADER -->

                <!-- CONTENT -->
                <div class="page-content">

                    <ul class="nav nav-tabs" id="myTab" role="tablist"> 
                        <li class="nav-item">
                            <a class="nav-link btn active" id="home-tab" data-toggle="tab" href="#home-tab-pane" role="tab" aria-controls="home-tab-pane" aria-expanded="true">Home</a>
                        </li> 
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active p-3" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab"> 
                         <div class="widget-group row no-gutters">

                        </div> 
                    </div>
                </div>
            </div>
            <!-- / CONTENT -->
            </div>     
        </div> 
    </div>
</div>
 

 

@endsection
