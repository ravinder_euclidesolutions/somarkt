@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="content custom-scrollbar">
  
        <div id="forgot-password" class="p-8">

            <div class="form-wrapper md-elevation-8 p-8">

                 <img  src="{{ asset('public/images/logos/somarktLogo.svg')}} " alt="logo" style="width: 150px;">
                 
                <div class="title mt-4 mb-8">Recover your password</div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <form name="forgotPasswordForm" novalidate  method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group mb-4">
                        <label for="forgotPasswordFormInputEmail">Email address</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="forgotPasswordFormInputEmail" aria-describedby="emailHelp" placeholder=" " required/>
                        
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="submit-button btn btn-block btn-secondary mt-8 mb-4 mx-auto" aria-label="SEND RESET LINK">
                        SEND RESET LINK
                    </button>

                </form>

                <div class="login row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                    <a class="link text-secondary" href="{{ route('login') }}">Go back to login</a>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection
