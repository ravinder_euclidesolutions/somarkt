<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head> 
  @include('layouts.head')

<script type="text/javascript">	
	$(window).load(function() {	
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body class="layout layout-vertical layout-left-navigation layout-above-toolbar layout-above-footer">
    <main>
        
        <div id="wrapper">
            @auth
            
               @include('layouts.sidebar')

            @endauth

            @section('content')

              @show

    @include('layouts.footer')