<aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
    <div class="aside-content bg-primary-700 text-auto">

        <div class="aside-toolbar">
            <div class="logo">
                <img src="{{ asset('public/images/logos/somarktLogo.svg')}} " alt="logo" style="width:40px">
                <span class="ml-2">SoMarkt</span>
            </div>
        </div>

        <ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">
            <li class="subheader">
                <span class="mr-5 profile">
                <img class="profile-image avatar medium " src="{{ asset('public/images/avatars/profile.jpg')}} " alt="avatar">
                    <i class="status text-green icon-checkbox-marked-circle s-4"></i>
                </span>
                <span>{{ auth()->user()->name }}</span>
            </li>

            <li class="nav-item" role="tab" id="heading-dashboards">

                <a class="nav-link ripple {{ Request::path() == 'home' ? 'active' : '' }} " href="{{ route('home') }}"  >
                    <i class="icon s-4 icon-tile-four"></i>
                    <span>Home</span>
                </a>

            </li>
            <li class="nav-item" role="tab" id="heading-users">

                <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-users" href="#" aria-expanded="false" aria-controls="collapse-users">
                    <i class="icon icon-account s-4"></i>
                    <span>ISO</span>
                </a>
                <ul id="collapse-users" class='collapse ' role="tabpanel" aria-labelledby="heading-users" data-children=".nav-item">
                    <li class="nav-item">
                        <a class="nav-link ripple {{ Request::path() == 'iso' ? 'active' : '' }}" href="{{ url('/admin/iso') }}"  >
                            <i class="icon-view-list"></i>
                            <span>All ISO</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="icon-logout s-4"></i>
                    <span>Logout</span>
                    <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
                </a>
            </li>
        </ul>
    </div>

</aside>
 <style type="text/css">
    .profile {
        position: relative;
    }
     .profile .status {
        position: absolute;
        bottom: -.4rem;
        right: -.4rem;
     }
 </style>

