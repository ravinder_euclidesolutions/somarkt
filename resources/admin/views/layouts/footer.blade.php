	</div>
	@auth
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">             
            <div id="copyright text-right">© {{ date('Y') }}  SoMarkt. All rights reserved. </div>
        </nav>
	@endauth        
</main>

<script type="text/javascript">    
    $(document).ready(function(){        
        $('.nav-item').find('.nav-link.active').parent().parent().addClass('show');
        $('.nav-item').find('.nav-link.active').parent().parent().prev().removeClass('collapsed');       
    });
</script>

</body>
<div class="se-pre-con"></div>

<style type="text/css">
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999; 
      background-image: url({{ asset('public/images/Preloader_2.gif')}});
      background-position: center;
      background-repeat: no-repeat;
      background-color: #ffffff33;
    }
</style>
</html>